import { ElementoGestionItemInterface } from './planificador/models/elemento-item.interface';
import { ObjetivoItemInterface } from './planificador/models/objetivo-item.interface';
import {Injectable} from '@angular/core';
import {PlanificadorDataServiceInterface} from './planificador/services/planificador-data.service';

@Injectable()
export class ServicioDeDatosService implements PlanificadorDataServiceInterface {

  constructor() {
  }

  getEmpresa() {
    return false;
  }

  getTitle() {
    return 'Plan de prevención';
  }

  getNiveles() {
    return [
      {title: 'Plan de prevencion', identifier: 'PDP', order: 0},
      {title: 'Nivel I', identifier: 'N1', order: 1},
      {title: 'Nivel II', identifier: 'N2', order: 2},
      {title: 'Nivel III', identifier: 'N3', order: 3},
      {title: 'Nivel IV', identifier: 'N4', order: 4},
      {title: 'Nivel V', identifier: 'N5', order: 5},
    ];
  }


  private createElements(): Array<ElementoGestionItemInterface> {
    const elementos: Array<ElementoGestionItemInterface> = [];

    for (let i = 0; i < 5; i++) {
      elementos.push({
        title: `Elemento ${i}`,
        description: `Descripción de evento ${i}`,
        identifier: i.toString()
      });
    }

    return elementos;
  }

  getObjetivos() {
    const objetivos: Array<ObjetivoItemInterface> = [];

    this.createElements().map((elemento, ) => {
      for (let i = 0; i < 5; i++) {

        let start = null;
        let end = null;

        if (Math.random() < 0.8) {
          start = this.getRandomDate(new Date(2017, 1, 1), new Date());
          end = this.getRandomDate(start, new Date());
          start = start.getTime();
          end = end.getTime();
        }

        objetivos.push({
          title: `Objetivo ${elemento.title} - ${i}`,
          description: `Descripción de Objetivo ${i}`,
          elemento: elemento,
          nivel: this.getNiveles()[Math.floor(Math.random() * this.getNiveles().length)],
          fechainicio: start,
          fechafin: end,
        });
      }
    });

    return objetivos;
  }

  getRandomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  }
}
