import { TestBed, inject } from '@angular/core/testing';

import { ServicioDeDatosService } from './servicio-de-datos.service';

describe('ServicioDeDatosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServicioDeDatosService]
    });
  });

  it('should be created', inject([ServicioDeDatosService], (service: ServicioDeDatosService) => {
    expect(service).toBeTruthy();
  }));
});
