import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {PlanificadorModule} from './planificador/planificador.module';
import {ServicioDeDatosService} from './servicio-de-datos.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PlanificadorModule.forRoot(ServicioDeDatosService)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
