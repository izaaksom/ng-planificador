import { FormsModule } from '@angular/forms';
import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PlanificadorComponent} from './components/planificador/planificador.component';
import {PLANIFICADOR_DATA_SERVICE} from './services/planificador-data.service';
import { IndicadorNivelesComponent } from './components/indicador-niveles/indicador-niveles.component';
import { IndicadorNivelItemComponent } from './components/indicador-nivel-item/indicador-nivel-item.component';
import { GanttPanelComponent } from './components/gantt-panel/gantt-panel.component';
import { GanttElementoComponent } from './components/gantt-elemento/gantt-elemento.component';
import { GanttRowComponent } from './components/gantt-row/gantt-row.component';
import { GanttFiltrosComponent } from './components/gantt-filtros/gantt-filtros.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    PlanificadorComponent,
    IndicadorNivelesComponent,
    IndicadorNivelItemComponent,
    GanttPanelComponent,
    GanttElementoComponent,
    GanttRowComponent,
    GanttFiltrosComponent,
  ],
  exports: [
    CommonModule,
    FormsModule,
    PlanificadorComponent
  ],
})
export class PlanificadorModule {

  /**
   * Module importer with configurable services.
   * @param {PlanificadorDataServiceInterface} dataService
   * @return {ModuleWithProviders}
   */
  public static forRoot(dataService): ModuleWithProviders {
    return {
      ngModule: PlanificadorModule,
      providers: [
        {provide: PLANIFICADOR_DATA_SERVICE, useClass: dataService}
      ]
    };
  }
}
