import { NivelGestionItemInterface } from './nivel-gestion-item.interface';
import { ElementoGestionItemInterface } from './elemento-item.interface';

export interface ObjetivoItemInterface {
    title: string;
    description?: string;
    elemento: ElementoGestionItemInterface;
    tipoDeObjetivo?: any;
    nivel: NivelGestionItemInterface;
    prioridad?: any;
    fechainicio?: number;
    fechafin?: number;
    responsable?: any;
}
