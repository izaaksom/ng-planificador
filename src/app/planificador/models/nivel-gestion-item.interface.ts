export interface NivelGestionItemInterface {
  title: string;
  description?: string;
  identifier: string;
  active?: boolean;
  order: number;
}
