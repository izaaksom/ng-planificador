import { ObjetivoItemInterface } from './objetivo-item.interface';
export interface ElementoGestionItemInterface {
  title: string;
  description?: string;
  objetivos?: Array<ObjetivoItemInterface>;
  identifier: string;
}
