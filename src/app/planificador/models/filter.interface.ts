import { ElementoGestionItemInterface } from './elemento-item.interface';
import { NivelGestionItemInterface } from './nivel-gestion-item.interface';


export enum PlanificadoEnum {
    si= 'si',
    no = 'no',
    todos = 'todos'
}
export interface Filter {
    nivel?: NivelGestionItemInterface;
    elemento?: ElementoGestionItemInterface;
    planificado: PlanificadoEnum;
}
