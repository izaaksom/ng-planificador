import { NivelGestionItemInterface } from '../../models/nivel-gestion-item.interface';
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import * as moment from 'moment';
import { ObjetivoItemInterface } from '../../models/objetivo-item.interface';

@Component({
  selector: 'app-gantt-row',
  templateUrl: './gantt-row.component.html',
})
export class GanttRowComponent implements OnInit {

  @Input() objetivo: ObjetivoItemInterface;
  @Input() nivel: NivelGestionItemInterface;

  private progress: number;

  constructor() { }

  ngOnInit() {
    this.progress = (Math.random() * 100);
  }

  getTitle() {
    return this.objetivo.title;
  }

  getNivel() {
    return this.objetivo.nivel.identifier;
  }

  getLeft() {
    const time = moment(this.objetivo.fechainicio);
    return (time.dayOfYear() * 100 / (time.isLeapYear() ? 366 : 365)).toString() + '%';
  }

  getWidth() {
    const timeInicio = moment(this.objetivo.fechainicio);
    const timeFin = moment(this.objetivo.fechafin);
    return ((timeFin.dayOfYear() - timeInicio.dayOfYear()) * 100 / (timeInicio.isLeapYear() ? 366 : 365)).toString() + '%';
  }

  getStyle() {
    return {
      left: this.getLeft(),
      width: this.getWidth()
    };
  }

  getProgressStyle() {
    return {
      width: this.progress.toString() + '%'
    };
  }

  getProgressClass() {
    return {
      'red': this.progress < 50,
      'yellow': this.progress >= 33 && this.progress < 100,
      'green': this.progress === 100,
    };
  }

  getFechaInicio() {
    return moment(this.objetivo.fechainicio).locale('es').calendar();
  }

  getFechaFin() {
    return moment(this.objetivo.fechafin).locale('es').calendar();
  }

  nivelActivo() {
    return {
      active: this.objetivo.nivel.identifier === this.nivel.identifier
    };
  }

  isPlaneado() {
    return this.objetivo.fechafin && this.objetivo.fechainicio;
  }

  getPlaneadoClass() {
    return {
      'no-planeado': !this.isPlaneado()
    };
  }
}
