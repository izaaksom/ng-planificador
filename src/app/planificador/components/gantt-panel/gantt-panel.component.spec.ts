import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GanttPanelComponent } from './gantt-panel.component';

describe('GanttPanelComponent', () => {
  let component: GanttPanelComponent;
  let fixture: ComponentFixture<GanttPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GanttPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GanttPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
