import { NivelGestionItemInterface } from '../../models/nivel-gestion-item.interface';
import { ElementoGestionItemInterface } from '../../models/elemento-item.interface';
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { ObjetivoItemInterface } from '../../models/objetivo-item.interface';

@Component({
  selector: 'app-gantt-panel',
  templateUrl: './gantt-panel.component.html',
})
export class GanttPanelComponent implements OnInit {

  @Input() objetivos: Array<ObjetivoItemInterface>;
  @Input() nivel: NivelGestionItemInterface;

  constructor() { }

  ngOnInit() {
  }

  getElementosSorted() {
    return this.getElementosSorted()
    .sort((a: ElementoGestionItemInterface,b: ElementoGestionItemInterface) => {
      return a.title.toLowerCase().localeCompare(b.title.toLowerCase());
    });
  }

  private getElementos(): Array<ElementoGestionItemInterface> {
    const elementos: Object = {};

    this.objetivos.map(obj => {
      if (!elementos.hasOwnProperty(obj.elemento.identifier)) {
        elementos[obj.elemento.identifier] = obj.elemento;
        elementos[obj.elemento.identifier].objetivos = [];
      }

      const elemento = elementos[obj.elemento.identifier];
      elemento.objetivos.push(obj);
    });

    return Object.keys(elementos)
      .map(key => (elementos[key]));
  }
}
