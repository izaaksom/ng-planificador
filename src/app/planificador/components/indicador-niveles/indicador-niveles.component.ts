import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { NivelGestionItemInterface } from '../../models/nivel-gestion-item.interface';

@Component({
  selector: 'app-indicador-niveles',
  templateUrl: './indicador-niveles.component.html',
})
export class IndicadorNivelesComponent implements OnInit {

  @Input() niveles: Array<NivelGestionItemInterface>;
  @Output() activation: EventEmitter<NivelGestionItemInterface> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  activationHandler(item: NivelGestionItemInterface) {
    this.activation.emit(item);
  }
}
