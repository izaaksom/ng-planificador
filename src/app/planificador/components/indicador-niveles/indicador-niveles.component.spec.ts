import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndicadorNivelesComponent } from './indicador-niveles.component';

describe('IndicadorNivelesComponent', () => {
  let component: IndicadorNivelesComponent;
  let fixture: ComponentFixture<IndicadorNivelesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndicadorNivelesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndicadorNivelesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
