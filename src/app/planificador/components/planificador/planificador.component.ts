import { Filter, PlanificadoEnum } from '../../models/filter.interface';
import { ElementoGestionItemInterface } from '../../models/elemento-item.interface';
import { Component, OnInit, ViewEncapsulation, Inject, Input } from '@angular/core';
import { PlanificadorParams } from '../../models/planificador-parameters.interface';
import { PlanificadorDataServiceInterface, PLANIFICADOR_DATA_SERVICE } from '../../services/planificador-data.service';
import { NivelGestionItemInterface } from '../../models/nivel-gestion-item.interface';
import { ObjetivoItemInterface } from '../../models/objetivo-item.interface';

@Component({
  selector: 'app-planificador',
  templateUrl: './planificador.component.html',
})
export class PlanificadorComponent implements OnInit {

  @Input() params: PlanificadorParams;

  private niveles: Array<NivelGestionItemInterface> = [];
  private objetivos: Array<ObjetivoItemInterface> = [];
  public filters: Filter = { planificado: PlanificadoEnum.todos };

  constructor(@Inject(PLANIFICADOR_DATA_SERVICE) private dataService: PlanificadorDataServiceInterface) { }

  ngOnInit() {
    this.initialize(this.params);
  }

  /**
   * Inicializa planificador basado en parametros de componente.
   * @param {PlanificadorParams} params
   */
  private initialize(params: PlanificadorParams) {
    this.niveles = this.dataService.getNiveles().map((nivel, index) => {
      nivel.order = index;
      return nivel;
    });

    this.niveles[0].active = true;
    this.setNivelFilter(this.niveles[0]);

    this.objetivos = this.dataService.getObjetivos();
  }

  /**
   * Retorna titulo de planificador.
   * @returns {string}
   */
  public getTitle() {
    return this.dataService.getTitle();
  }

  /**
   * Retorna con todos los niveles de planificador.
   * @returns {Array<NivelGestionItemInterface>}
   */
  public getNiveles() {
    return this.niveles;
  }

  /**
   * Activa un cambio de nivel.
   * @param {NivelGestionItemInterface} item Item de nivel.
   */
  public activeNivel(item: NivelGestionItemInterface) {
    this.niveles.map((nivel) => {
      if (nivel.identifier === item.identifier) {
        nivel.active = true;
        this.setNivelFilter(nivel);
        return;
      }
      nivel.active = false;
    });
  }

  private setNivelFilter(nivel: NivelGestionItemInterface) {
    this.filters['nivel'] = nivel;
  }

  private applyFilters(): Array<ObjetivoItemInterface> {
    console.log(this.filters);
    return this.objetivos.filter( obj => {
      return obj.nivel.order <= this.filters.nivel.order;
    }).filter(obj => {
      switch (this.filters.planificado) {
        case PlanificadoEnum.si:
        return obj.fechafin && obj.fechainicio;
        case PlanificadoEnum.no:
          return !obj.fechafin && !obj.fechainicio;
        case PlanificadoEnum.todos:
        default:
        return true;
      }
    });
  }

  public getObjetivos() {
    return this.applyFilters();
  }

  public getActiveNivel() {
    return this.niveles.filter( nivel => (nivel.active))[0];
  }
}
