import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { NivelGestionItemInterface } from '../../models/nivel-gestion-item.interface';

@Component({
  selector: 'app-indicador-nivel-item',
  templateUrl: './indicador-nivel-item.component.html',
})
export class IndicadorNivelItemComponent implements OnInit {

  @Input() nivel: NivelGestionItemInterface;
  @Output() activation: EventEmitter<NivelGestionItemInterface> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  activate() {
    this.activation.emit(this.nivel);
  }

  getClasses() {
    return {
      active: this.nivel.active
    };
  }

}
