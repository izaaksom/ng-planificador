import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndicadorNivelItemComponent } from './indicador-nivel-item.component';

describe('IndicadorNivelItemComponent', () => {
  let component: IndicadorNivelItemComponent;
  let fixture: ComponentFixture<IndicadorNivelItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndicadorNivelItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndicadorNivelItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
