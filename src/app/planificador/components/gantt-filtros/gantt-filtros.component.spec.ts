import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GanttFiltrosComponent } from './gantt-filtros.component';

describe('GanttFiltrosComponent', () => {
  let component: GanttFiltrosComponent;
  let fixture: ComponentFixture<GanttFiltrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GanttFiltrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GanttFiltrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
