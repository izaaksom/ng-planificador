import { Component, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Filter } from '../../models/filter.interface';

@Component({
  selector: 'app-gantt-filtros',
  templateUrl: './gantt-filtros.component.html',
})
export class GanttFiltrosComponent implements OnInit {

  @Input() @Output() filtro: Filter;
  private active = false;

  constructor() { }

  ngOnInit() {
    console.log(this.filtro);
  }

  activateToggle() {
    this.active = !this.active;
    return this.active;
  }

}
