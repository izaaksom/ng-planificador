import { NivelGestionItemInterface } from '../../models/nivel-gestion-item.interface';
import { ObjetivoItemInterface } from '../../models/objetivo-item.interface';
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { ElementoGestionItemInterface } from '../../models/elemento-item.interface';

@Component({
  selector: 'app-gantt-elemento',
  templateUrl: './gantt-elemento.component.html'
})
export class GanttElementoComponent implements OnInit {

  @Input() public elemento: ElementoGestionItemInterface;
  @Input() public nivel: NivelGestionItemInterface;

  constructor() { }

  ngOnInit() {
  }

  getObjetivos() {
    return this.elemento.objetivos
      .sort((a: ObjetivoItemInterface, b: ObjetivoItemInterface) => {
        return a.nivel.order < b.nivel.order || (a.nivel.order === b.nivel.order && a.title < b.title) ? -1 : 1;
      });
  }
}
