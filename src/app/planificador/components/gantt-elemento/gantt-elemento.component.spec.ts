import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GanttElementoComponent } from './gantt-elemento.component';

describe('GanttElementoComponent', () => {
  let component: GanttElementoComponent;
  let fixture: ComponentFixture<GanttElementoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GanttElementoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GanttElementoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
