import { ObjetivoItemInterface } from '../models/objetivo-item.interface';
import {InjectionToken} from '@angular/core';
import { NivelGestionItemInterface } from '../models/nivel-gestion-item.interface';


export interface PlanificadorDataServiceInterface {
  getEmpresa(): any;

  getNiveles(): Array<NivelGestionItemInterface>;

  getObjetivos(): Array<ObjetivoItemInterface>;

  getTitle(): string;
}

export const PLANIFICADOR_DATA_SERVICE = new InjectionToken<PlanificadorDataServiceInterface>('Servicio de datos del planificador');
